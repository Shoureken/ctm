package conference.track.management.utils;

import static java.util.Calendar.MINUTE;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import conference.track.management.business.Track;
import conference.track.management.to.Conference;
import conference.track.management.to.ScheduleTalk;
import conference.track.management.to.Talk;

public class ConferencePrinter {

    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("hh:mmaaa");

    public static void print(Conference conference) {
	final List<Track> tracks = conference.getTracks();
	for (int i = 0; i < tracks.size(); i++) {
	    System.out.println(String.format("Track %d", i + 1));
	    final Track track = tracks.get(i);
	    for (Iterator<ScheduleTalk> iterator = track.iterator(); iterator.hasNext(); ) {
		final ScheduleTalk scheduleTalk = iterator.next();
		final Talk talk = scheduleTalk.getTalk();
		final String name = talk.getName();
		final int startTime = scheduleTalk.getStartTime();
		final String formatMinutes = formatMinutes(startTime);
		System.out.println(formatMinutes + " " + name);
	    }
	    System.out.println("");
	}

    }

    private static String formatMinutes(int minutes) {
	final Calendar instance = Calendar.getInstance();
	instance.set(0, 0, 0, 0, 0, 0);
	instance.add(MINUTE, minutes);
	final Date time = instance.getTime();
	return FORMATTER.format(time);
    }

}
