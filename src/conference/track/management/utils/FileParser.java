package conference.track.management.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import conference.track.management.to.Talk;

public class FileParser {

    private static final String NAME = "name";
    private static final String DURATION = "duration";
    private static final String TIME = "time";
    private static final String LIGHTNING = "lightning";
    private static final String REGEX = "(?<"
					+ NAME
					+ ">.*\\s(?<"
					+ DURATION
					+ ">(?<"
					+ TIME
					+ ">\\d+)(min)|("
					+ LIGHTNING
					+ ")))";
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    public List<Talk> read(String path) throws IOException {
	final FileReader fileReader = new FileReader(path);
	final BufferedReader bufferedReader = new BufferedReader(fileReader);
	String line;
	int count = 1;
	final List<Talk> result = new ArrayList<>();
	while ((line = bufferedReader.readLine()) != null) {
	    final Talk talk = parseLine(line, count);
	    count++;
	    if (talk == null) {
		continue;
	    }
	    result.add(talk);
	}
	return result;
    }

    private Talk parseLine(String line, int count) {
	final Matcher matcher = PATTERN.matcher(line);
	if (!matcher.matches()) {
	    System.out.println("Impossivel ler o arquivo");
	    System.out.println(String.format("Impossivel ler a linha %d:%s", count, line));
	    return null;
	}
	final String name = matcher.group(NAME);
	final String duration = matcher.group(DURATION);
	if (LIGHTNING.equalsIgnoreCase(duration)) {
	    return new Talk(name, 5);
	}
	final String timeString = matcher.group(TIME);
	final int time = Integer.parseInt(timeString);
	return new Talk(name, time);
    }

}
