package conference.track.management.exception;

public class InsufficientLenghtExpcetion extends Exception {

    private final int totalLenght;

    public InsufficientLenghtExpcetion(int totalLenght) {
	super("Insufficient lenght");
	this.totalLenght = totalLenght;
    }

    public int getTotalLenght() {
	return totalLenght;
    }

}
