package conference.track.management.exception;

public class TrackFullException extends Exception {

    public TrackFullException() {
	super("This Track is full");
    }
}
