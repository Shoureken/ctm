package conference.track.management.exception;

public class NoRoomException extends Exception {

    public NoRoomException() {
	super("No room for this talk");
    }

}
