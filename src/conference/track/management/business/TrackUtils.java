package conference.track.management.business;

import conference.track.management.to.ScheduleTalk;
import conference.track.management.to.Talk;

interface TrackUtils {

    /**
     * @param a the first schedule
     * @param b the second schedule
     * @return true if a is right before b
     */
    static boolean isARightBeforeB(ScheduleTalk a, ScheduleTalk b) {
	return a.getFinishTime() == b.getStartTime();
    }

    static boolean hasRoomBetweenFor(int first, ScheduleTalk second, Talk talk) {
	return getRoomBetween(first, second) >= talk.getDuration();
    }

    static boolean hasRoomBetweenFor(ScheduleTalk first, int second, Talk talk) {
	return getRoomBetween(first, second) >= talk.getDuration();
    }

    static boolean hasRoomBetweenFor(ScheduleTalk first, ScheduleTalk second, Talk talk) {
	return getRoomBetween(first, second) >= talk.getDuration();
    }

    static boolean hasRoomBetween(int first, int second) {
	return getRoomBetween(first, second) > 0;
    }

    static int getRoomBetween(int first, int second) {
	return second - first;
    }

    static int getRoomBetween(ScheduleTalk first, int second) {
	return getRoomBetween(first.getFinishTime(), second);
    }

    static int getRoomBetween(ScheduleTalk first, ScheduleTalk second) {
	return getRoomBetween(first.getFinishTime(), second.getStartTime());
    }

    static int getRoomBetween(int first, ScheduleTalk second) {
	return getRoomBetween(first, second.getStartTime());
    }

    static boolean hasRoomBetween(int first, ScheduleTalk second) {
	return hasRoomBetween(first, second.getStartTime());
    }

    static boolean hasRoomBetween(ScheduleTalk first, int second) {
	return hasRoomBetween(first.getStartTime(), second);
    }

    static boolean hasRoomBetween(ScheduleTalk first, ScheduleTalk second) {
	return hasRoomBetween(first.getFinishTime(), second.getStartTime());
    }

}
