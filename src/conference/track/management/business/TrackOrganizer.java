package conference.track.management.business;

import static java.lang.Integer.MAX_VALUE;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import conference.track.management.exception.FilledAfternoonException;
import conference.track.management.exception.InsufficientLenghtExpcetion;
import conference.track.management.exception.NoRoomException;
import conference.track.management.exception.TrackFullException;
import conference.track.management.exception.UnfitableAfternoonException;
import conference.track.management.exception.UnfitableMorningException;
import conference.track.management.to.Conference;
import conference.track.management.to.Talk;

public class TrackOrganizer {

    private static Talk getLongest(List<Talk> talks, int shorterThan) {
	for (Talk talk : talks) {
	    if (talk.getDuration() < shorterThan) {
		return talk;
	    }
	}
	return null;
    }

    private void addRecursivelyMinimum(Track currentTrack, int duration, List<Talk> talks)
		    throws TrackFullException, UnfitableMorningException, FilledAfternoonException {
	try {
	    final Talk added = addRecursively(currentTrack, duration, false, talks);
	    talks.remove(added);
	} catch (UnfitableMorningException e) {
	    final Talk talk = currentTrack.removeLastScheduleBeforeLunch();
	    if (talk == null) {
		throw e;
	    }
	    talks.add(talk);
	    final int talkDuration = talk.getDuration();
	    addRecursivelyMinimum(currentTrack, talkDuration, talks);
	}
    }

    private static Talk addRecursively(Track currentTrack, int duration, boolean useAfternoonEnding, List<Talk> talks)
		    throws TrackFullException, UnfitableMorningException, FilledAfternoonException {
	final Talk longest = getLongest(talks, duration);
	if (longest == null) {
	    if (currentTrack.hasRoomBeforeLunch()) {
		throw new UnfitableMorningException();
	    }
	    throw new FilledAfternoonException();
	}
	try {
	    currentTrack.add(longest, useAfternoonEnding);
	    return longest;
	} catch (NoRoomException e) {
	    // If there was no room, we try a shorter one
	    return addRecursively(currentTrack, longest.getDuration(), useAfternoonEnding, talks);
	}
    }

    public Conference organize(List<Talk> talks)
		    throws UnfitableMorningException, InsufficientLenghtExpcetion, UnfitableAfternoonException {
	talks.sort(new ComparatorTalkDurationDesc());

	final List<Track> result = new ArrayList<>();
	try {
	    organize(talks, result);
	} catch (InsufficientLenghtExpcetion e) {
	    final int totalLenght = e.getTotalLenght();
	    final int totalRemaningTime = getTotalRemaningTime(result);
	    if (totalRemaningTime < totalLenght) {
		throw e;
	    }
	    fillAfternoonEnds(talks, result);
	}
	for (Track track : result) {
	    track.closeTrack();
	}
	return new Conference(result);
    }

    private void organize(List<Talk> talks, List<Track> result)
		    throws InsufficientLenghtExpcetion, UnfitableMorningException {
	Track currentTrack = getNewTrack(result, talks);
	while (!talks.isEmpty()) {
	    try {
		addRecursivelyMinimum(currentTrack, MAX_VALUE, talks);
	    } catch (TrackFullException | FilledAfternoonException e) {
		// If current track is full, we have to start a new one
		currentTrack = getNewTrack(result, talks);
	    }
	}
    }

    private void fillAfternoonEnds(List<Talk> talks, List<Track> result)
		    throws UnfitableMorningException, UnfitableAfternoonException {
	result.sort(new ComparatorTrackRemainninDesc());
	Track currentTrack = result.get(0);
	while (!talks.isEmpty()) {
	    try {
		final Talk added = addRecursively(currentTrack, MAX_VALUE, true, talks);
		talks.remove(added);
	    } catch (TrackFullException | FilledAfternoonException e) {
		final int index = result.indexOf(currentTrack) + 1;
		if (index < result.size()) {
		    currentTrack = result.get(index);
		    continue;
		}
		throw new UnfitableAfternoonException();
	    }
	}
    }

    private Track getNewTrack(List<Track> result, List<Talk> talks) throws InsufficientLenghtExpcetion {
	final int totalLenght = getTotalLenght(talks);
	final Track currentTrack = new Track();
	final int minimumTalkLenghtsToFill = currentTrack.getMinimumTalkLenghtToFill();
	if (totalLenght > minimumTalkLenghtsToFill) {
	    result.add(currentTrack);
	    return currentTrack;
	}
	throw new InsufficientLenghtExpcetion(totalLenght);
    }

    private static int getTotalRemaningTime(List<Track> tracks) {
	int total = 0;
	for (Track track : tracks) {
	    total += track.getRemainingFreeTimeUntilEnd();
	}
	return total;
    }

    private static int getTotalLenght(List<Talk> talks) {
	int totalLenght = 0;
	for (Talk talk : talks) {
	    totalLenght += talk.getDuration();
	}
	return totalLenght;
    }

    private class ComparatorTrackRemainninDesc implements Comparator<Track> {

	@Override
	public int compare(Track o1, Track o2) {
	    return Integer.compare(o2.getRemainingFreeTimeUntilEnd(), o1.getRemainingFreeTimeUntilEnd());
	}

    }

    private class ComparatorTalkDurationDesc implements Comparator<Talk> {

	@Override
	public int compare(Talk o1, Talk o2) {
	    return Integer.compare(o2.getDuration(), o1.getDuration());
	}

    }

}
