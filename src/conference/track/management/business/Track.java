package conference.track.management.business;

import static conference.track.management.business.TrackUtils.getRoomBetween;
import static conference.track.management.business.TrackUtils.hasRoomBetween;
import static conference.track.management.business.TrackUtils.hasRoomBetweenFor;
import static conference.track.management.business.TrackUtils.isARightBeforeB;
import static conference.track.management.utils.DateUtils.getMinutes;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import conference.track.management.exception.NoRoomException;
import conference.track.management.exception.TrackFullException;
import conference.track.management.to.LunchSchedule;
import conference.track.management.to.NetworkingTalk;
import conference.track.management.to.ScheduleTalk;
import conference.track.management.to.Talk;

public class Track {

    private final int morningStart = getMinutes(9, 0);
    private final int afternoonEarlyEnds = getMinutes(16, 0);
    private final int afternoonLaterEnds = getMinutes(17, 0);
    private final LunchSchedule lunchSchedule = new LunchSchedule();
    private final TreeSet<ScheduleTalk> schedules = new TreeSet<>(new ScheduleTalkComparator());

    public Track() {
	schedules.add(lunchSchedule);
    }

    public Talk removeLastScheduleBeforeLunch() {
	ScheduleTalk lastScheduleBeforeLunch = getLastScheduleBeforeLunch();
	if (lastScheduleBeforeLunch == null) {
	    return null;
	}
	schedules.remove(lastScheduleBeforeLunch);
	return lastScheduleBeforeLunch.getTalk();
    }

    public ScheduleTalk add(Talk talk, boolean useAfternoonEnd) throws NoRoomException, TrackFullException {
	final ScheduleTalk lastScheduleBeforeLunch = getLastScheduleBeforeLunch();
	if (lastScheduleBeforeLunch == null) {
	    if (hasRoomBetweenFor(morningStart, lunchSchedule, talk)) {
		return schedule(talk, morningStart);
	    }
	    throw new NoRoomException();
	}

	if (hasRoomBetween(lastScheduleBeforeLunch, lunchSchedule)) {
	    if (hasRoomBetweenFor(lastScheduleBeforeLunch, lunchSchedule, talk)) {
		return scheduleAfter(talk, lastScheduleBeforeLunch);
	    }
	    throw new NoRoomException();
	}

	final ScheduleTalk lastSchedule = getLastSchedule();

	if (!hasRoomBetween(lastSchedule, afternoonLaterEnds)) {
	    throw new TrackFullException();
	}

	// Check if has room in the end of the track
	if (hasRoomBetweenFor(lastSchedule, afternoonEarlyEnds, talk)) {
	    return scheduleAfter(talk, lastSchedule);
	}

	if (!hasRoomBetweenFor(lastSchedule, afternoonLaterEnds, talk)){
	    throw new NoRoomException();
	}

	if (useAfternoonEnd || lastSchedule.getFinishTime() < afternoonEarlyEnds) {
	    return scheduleAfter(talk, lastSchedule);
	}
	throw new NoRoomException();
    }

    private ScheduleTalk scheduleAfter(Talk talk, ScheduleTalk lastSchedule) {
	return schedule(talk, lastSchedule.getFinishTime());
    }

    public void closeTrack() {
	final ScheduleTalk lastSchedule = getLastSchedule();
	final int duration = getRoomBetween(lastSchedule, afternoonLaterEnds);
	scheduleAfter(new NetworkingTalk(duration), lastSchedule);
    }

    private ScheduleTalk getLastSchedule() {
	return schedules.last();
    }

    private ScheduleTalk getLastScheduleBeforeLunch() {
	ScheduleTalk last = null;
	for (ScheduleTalk schedule : schedules) {
	    if (isLunchSchedule(schedule)) {
		return last;
	    }
	    last = schedule;
	}
	return null;
    }

    public int getRemainingFreeTimeUntilEnd() {
	ScheduleTalk lastSchedule = getLastSchedule();
	return getRoomBetween(lastSchedule, afternoonLaterEnds);
    }

    public int getMinimumTalkLenghtToFill() {
	return getRoomBetween(morningStart, lunchSchedule) + getRoomBetween(lunchSchedule, afternoonEarlyEnds);
    }

    public boolean hasRoomBeforeLunch() {
	if (!hasRoomBetween(morningStart, lunchSchedule)) {
	    return false;
	}
	final ScheduleTalk lastScheduleBeforeLunch = getLastScheduleBeforeLunch();
	return lastScheduleBeforeLunch == null || !isARightBeforeB(lastScheduleBeforeLunch, lunchSchedule);
    }

    private boolean isLunchSchedule(ScheduleTalk schedule) {
	return schedule == lunchSchedule;
    }

    private ScheduleTalk schedule(Talk talk, int startTime) {
	final ScheduleTalk scheduleTalk = new ScheduleTalk(talk, startTime);
	schedules.add(scheduleTalk);
	return scheduleTalk;
    }

    public Iterator<ScheduleTalk> iterator() {
	return schedules.iterator();
    }

    private class ScheduleTalkComparator implements Comparator<ScheduleTalk> {

	@Override
	public int compare(ScheduleTalk o1, ScheduleTalk o2) {
	    final Integer startTime1 = o1.getStartTime();
	    final Integer startTime2 = o2.getStartTime();
	    return startTime1.compareTo(startTime2);
	}
    }

}
