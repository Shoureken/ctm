package conference.track.management.to;

import java.util.List;

import conference.track.management.business.Track;

public class Conference {

    private final List<Track> tracks;

    public Conference(List<Track> tracks) {
	this.tracks = tracks;
    }

    public List<Track> getTracks() {
	return tracks;
    }

}
