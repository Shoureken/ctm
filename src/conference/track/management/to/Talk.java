package conference.track.management.to;

public class Talk {

    private final String name;
    private final int duration;

    public Talk(String name, int duration) {
	this.name = name;
	this.duration = duration;
    }

    public String getName() {
	return name;
    }

    public int getDuration() {
	return duration;
    }

    @Override
    public String toString() {
	return "Talk{" + "name='" + name + '\'' + ", duration=" + duration + '}';
    }

}

