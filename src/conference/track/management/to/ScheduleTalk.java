package conference.track.management.to;

public class ScheduleTalk {

    private final Talk talk;
    private final int startTime;

    public ScheduleTalk(Talk talk, int startTime) {
	this.talk = talk;
	this.startTime = startTime;
    }

    public Talk getTalk() {
	return talk;
    }

    public int getStartTime() {
	return startTime;
    }

    public int getFinishTime() {
	return startTime + talk.getDuration();
    }

    @Override
    public String toString() {
	return "ScheduleTalk{" + "talk=" + talk + ", startTime=" + startTime + '}';
    }
}
