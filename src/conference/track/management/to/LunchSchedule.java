package conference.track.management.to;

import static conference.track.management.utils.DateUtils.getMinutes;

public class LunchSchedule extends ScheduleTalk {

    public LunchSchedule() {
	super(new Talk("Lunch", 60), getMinutes(12, 0));
    }

}
