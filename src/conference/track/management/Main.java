package conference.track.management;

import static conference.track.management.utils.ConferencePrinter.print;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import conference.track.management.exception.UnfitableAfternoonException;
import conference.track.management.exception.InsufficientLenghtExpcetion;
import conference.track.management.exception.UnfitableMorningException;
import conference.track.management.to.Conference;
import conference.track.management.utils.FileParser;
import conference.track.management.to.Talk;
import conference.track.management.business.TrackOrganizer;

public class Main {

    private static final String FILENAME = "input.txt";

    public static void main(String[] args) {
	final String path = getInputPath(args);
	final List<Talk> talkList;
	try {
	    talkList = new FileParser().read(path);
	} catch (FileNotFoundException e) {
	    System.out.println(String.format("Arquivo %s não encontrado", path));
	    return;
	} catch (IOException e) {
	    System.out.println("Impossivel ler o arquivo");
	    return;
	}
	final Conference conference;
	try {
	    conference = new TrackOrganizer().organize(talkList);
	} catch (UnfitableMorningException e) {
	    System.out.println("Impossivel organizar as palestras de maneira que preencha manhã");
	    return;
	}catch (UnfitableAfternoonException e) {
	    System.out.println("Impossivel organizar as palestras de maneira que preencha a tarde");
	    return;
	} catch (InsufficientLenghtExpcetion e) {
	    System.out.println("A duração das palestras é insufiente pra preencher a sessão");
	    return;
	}
	print(conference);
    }

    private static String getInputPath(String[] args) {
	if (args == null || args.length <= 0) {
	    return FILENAME;
	}
	return args[0];
    }

}
